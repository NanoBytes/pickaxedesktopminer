const electron = require('electron')
const ipc = electron.ipcRenderer

let current_app_version = "0.0.0"
let current_xmrig_version = 0
let finished_files = 0;

let latest_xmrig_version = -1

ipc.send("get_app_version");
ipc.on("app_version_returned", function(event, arg) {
    document.getElementById("app-version").innerHTML = arg[0]
    document.getElementById("miner-version").innerHTML = arg[1]

    current_app_version = arg[0]
    current_xmrig_version = arg[1]
})

ipc.send("get_latest_versions");
ipc.on("latest_versions_returned", function(event, arg) {
    if(arg == "Could not connect to the internet") {
        document.getElementById("status_message").innerHTML = arg;
        return
    }
    if(arg[2] == "win32") {
        document.getElementById("status_message").innerHTML = "Pickaxe only works on 64 bit Windows";
        return
    }
    
    if(current_app_version != arg[0] && !current_app_version.includes("beta")) {
        document.getElementById("status_message").innerHTML = "There is a new version available (" +arg[0]+
            ")<br><a style='color: grey; text-decoration: underline;' target=_blank href='https://pickaxeminer.us/download'>https://pickaxeminer.us/download</a>";
    } else {
        ipc.send("log", "Current miner: "+current_xmrig_version+", latest: "+arg[1] )
        latest_xmrig_version = arg[1]
        if(current_xmrig_version != arg[1]) {
            console.log("Current miner: "+current_xmrig_version+", latest: "+arg[1])
            finished_files = 0;
            ipc.send("download_miner", { properties: {} });
        } else {
            ipc.send("check_for_executable");
        }
    }

    document.getElementById("log_location").innerHTML = arg[3]
})

ipc.on("start_downloading", (event, progress) => {
    document.getElementById("status_message").innerHTML = "Downloading miner";
    document.getElementById("progress_bar").style.visibility = "visible";
});

ipc.on("move_download_bar", (event, progress) => {
    const progressInPercentages = progress * 100; // With decimal point and a bunch of numbers
    const cleanProgressInPercentages = Math.floor(progress * 100); // Without decimal point
    percent = cleanProgressInPercentages;
    document.getElementById("download_progress").style.width = cleanProgressInPercentages+"%";
    
});

ipc.on("download_complete", function(event, args) {
    console.log("Finished downloading: "+args[0])
    finished_files++
    if(finished_files == args[1]) {
        let location = args[0]
        document.getElementById("status_message").innerHTML = "Done downloading miner";
        ipc.send("set_xmrig_location", {location, latest_xmrig_version});
        ipc.send("check_for_executable");
    } else ipc.send("download_miner", { properties: {} });
})

ipc.on("download_failed", function(event, location) {
    document.getElementById("status_message").innerHTML = "Download failed<br>Try restarting the app";
    document.getElementById("status_message").style.color = "red";
})

ipc.on("results_of_xmrig_lookup", function(event, found) {
    finished_files = 0;
    if(found == false) ipc.send("download_miner", { properties: {} });
    else {
        setTimeout(() => {  
            document.getElementById("status_message").innerHTML = "All good!";
            document.getElementById("splash").style.visibility = "hidden";
            document.getElementById("progress_bar").style.visibility = "hidden";
            document.getElementById("miner").style.visibility = "visible";
        }, 2000);
        
    }


})
