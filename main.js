const electron = require('electron')
const app = electron.app
const BrowserWindow = electron.BrowserWindow
const ipc = electron.ipcMain
const log = require('electron-log');


const path = require('path')
const url = require('url')
const request = require('request');  
const Store = require('./store.js');
const fs = require('fs');
var os = require("os");
var sender;

const store = new Store({
    configName: 'user-preferences',
    defaults: {
      windowBounds: { width: 600, height: 700 },
      minerVersion: "0.0.0",
      xmrigLocation: ".",
      email: "",
      threads: 1,
      hasBenchmarked: false,
      amdGpu: false,
      nvidiaGpu: false
    }
  });

const userDataPath = (electron.app || electron.remote.app).getPath('userData');
log.transports.file.resolvePath = () =>  path.join(userDataPath, 'log.log')
process.env.NODE_TLS_REJECT_UNAUTHORIZED = 0;

const {download} = require("electron-dl")

let current_download = 0;
var cpu = os.cpus()[0]["model"]
var needed_binary = "win64"
if(cpu.includes("Apple")) needed_binary = "apple";
else if(process.platform == "darwin") needed_binary = "mac64"

if(needed_binary == "win64") 
    if(!process.env.PROCESSOR_ARCHITECTURE.includes("AMD64"))
        needed_binary = "win32"

const files_needed = ["config.json", needed_binary == "win64" ? "xmrig.exe" : "xmrig"]

let win;
function createWindow () {
    let { width, height } = store.get('windowBounds');
    win = new BrowserWindow({
        width: width, 
        height: height, 
        resizable: false,
        webPreferences: {
            nodeIntegration: true,
            contextIsolation: false,
            enableRemoteModule: true,
          }})
          //win.webContents.openDevTools()

    win.on('resize', () => {
        let { width, height } = win.getBounds();
        store.set('windowBounds', { width, height });
    });


    // andoad the index.html of the app.
    win.loadURL(url.format({
        pathname: path.join(__dirname, 'index.html'),
        protocol: 'file:',
        slashes: true
    }))
}

app.on('ready', createWindow)

// Quit when all windows are closed.
app.on('window-all-closed', () => {
    if (process.platform !== 'darwin') {
        app.quit()
    }
})

// Recreate window on MacOS dock click
app.on('activate', () => {
    if (win === null) {
        createWindow()
    }
})

ipc.on("log", function(event, arg) {
    log.info(arg)
})

ipc.on("done_benchmarking", function(event, arg) {
    store.set("hasBenchmarked", true)
})

process.on('uncaughtException', function (error) {
    log.error(error)
    sender.send("critical_error", "Your Antivirus has blocked cryptocurrency mining on your computer.\n\n\nPlease disable it and restart pickaxe to continue")
    store.set("minerVersion", "0.0.0")
    sender.send("stop_mining")
})


{ // Splash Screen IPC
    ipc.on("get_app_version", function(event, arg) {
        event.sender.send("app_version_returned", [app.getVersion(), store.get('minerVersion')] )
    })

    ipc.on("get_latest_versions", function(event, arg) {
        request('http://pickaxeminer.us/api/version', function (error, response, body) {
            if (!error && response.statusCode == 200) {
                let json_response = JSON.parse(body);
                log_location = path.join(userDataPath, 'log.log')
                event.sender.send("latest_versions_returned", [json_response.desktop_version, json_response.desktop_xmrig, needed_binary, log_location])
            } else {
                event.sender.send("latest_versions_returned", "Could not connect to the internet")

            }
        })
        
    })

    ipc.on("download_miner", function(event, arg) {
        let url = "https://bitbucket.org/NanoBytes/xmrig-miners/raw/main/"+needed_binary+"/";
        log.info(url)

        arg.properties.onStarted = function(status) {
            win.webContents.send("start_downloading", status.percent)
        }
        arg.properties.onTotalProgress = function(status) {
            win.webContents.send("move_download_bar", status.percent)
        }
        arg.properties.overwrite = true
        arg.properties.directory = path.join(app.getPath('appData'), "pickaxe")
        arg.properties.saveAs = false

        let file = files_needed[current_download]
        log.info("Downloading "+file)
        store.set("hasBenchmarked", false)
        store.set("amdGpu", false)
        get_config(event, arg)
        //arg.properties.filename = file
        download(BrowserWindow.getFocusedWindow(), url+file, arg.properties)
            .then(dl => win.webContents.send("download_complete", [dl.getSavePath(), files_needed.length]))
            .catch(err => win.webContents.send("download_failed", err));
        current_download++
    })

    ipc.on("set_xmrig_location", function(event, arg) {
        store.set("xmrigLocation", path.dirname(arg.location))
        store.set("minerVersion", arg.latest_xmrig_version)
    })

    ipc.on("check_for_executable", function(event, arg) {
        let all_files_found = true;
        let xmrig_location = store.get("xmrigLocation")
        for (const file of files_needed) {
            let file_location = path.join(xmrig_location, file)
            log.info("Looking for: "+file_location)
            if (!fs.existsSync(file_location)) {
                all_files_found = false;
                log.info("Could not find: "+file_location)
            } else fs.chmodSync(file_location, '777');
        }

        event.sender.send("results_of_xmrig_lookup", all_files_found )
    })
}

{ // Main View IPC

    var child;
    ipc.on("start_mining", function(event, arg) {
        try {
            sender = event.sender
            start_xmrig(arg)
        } catch(e) {
            
        }
    })

    function start_xmrig(arg) {
        var spawn = require('child_process').spawn;

        const xmrig_location = path.join(store.get("xmrigLocation"), "xmrig")
        log.info("Starting to mine with: "+xmrig_location)

        xmrig_arguments = build_arguments(arg[0], arg[1])
        log.info(xmrig_arguments)
        child = spawn(xmrig_location, xmrig_arguments);

        var scriptOutput = "";
        child.stdout.setEncoding('utf8');
        child.stdout.on('data', function(data) {
            log.silly('stdout: ' + data);
            data=data.toString();
            sender.send("xmrig_output", data )
        });
    }

    function build_arguments(email, threads) {
        const fileName = path.join(store.get("xmrigLocation"), "config.json");
        const file = require(fileName);
        if(store.get("hasBenchmarked") == false) {
            file["rebench-algo"] = true;
        } else {
            file["rebench-algo"] = false;
        }

        fs.writeFile(fileName, JSON.stringify(file, null, 2), function writeJSON(err) {
        });

        return [
                "-c",path.join(store.get("xmrigLocation"), "config.json"),
                "-o", "pickaxepool.us:5555",
                "-u", email,
                "-t", threads,
                "-p", "",
                "--no-color",
                "--bench-algo-time=10",
                "--max-cpu-usage=100",
                "--cpu-priority=3",
                "--retries=10000",
                "--print-time=3",
                "--donate-level=" + get_donation_amount(),
                store.get("hasBenchmarked") == false ? "--rebench-algo" : ""
        ]
    }

    function get_donation_amount() {
        return "3";
    }
    
    ipc.on("stop_mining", function(event, arg) {
        child.stdin.pause();
        child.kill();  
    })

    ipc.on("save_email", function(event, arg) {
        store.set("email", arg)
    })

    ipc.on("save_threads", function(event, arg) {
        store.set("threads", arg)
    })

    /*
    ipc.on("save_amd_gpu", function(event, arg) {
        store.set("amdGpu", arg)
        const fileName = path.join(store.get("xmrigLocation"), "config.json");
        const file = require(fileName);
        if(arg == "true") {
            file.opencl.enabled = true;
            file.opencl.cache = true;
            file.opencl["cn-lite/0"] = true;
            file.opencl["cn/0"]  = true;
            file.opencl["kawpow"]  = false;
            file.opencl["rx/graft"] = false;
            file.opencl["rx/arq"] = false;
            file.opencl["rx"] = false;
        } else {
            file.opencl.enabled = false;
        }

        fs.writeFile(fileName, JSON.stringify(file, null, 2), function writeJSON(err) {
          });
    })

    ipc.on("save_nvidia_gpu", function(event, arg) {
        store.set("nvidiaGpu", arg)
        const fileName = path.join(store.get("xmrigLocation"), "config.json");
        const file = require(fileName);
        if(arg == "true") {
            file.cuda.enabled = true;
        } else {
            file.opencl.enabled = false;
        }

        fs.writeFile(fileName, JSON.stringify(file, null, 2), function writeJSON(err) {
          });
    })*/

    ipc.on("set_bench", function(event, arg) {
        store.set("hasBenchmarked", arg)
    })

    ipc.on("get_user_config", function(event, arg) {
        get_config(event, arg)
    })

    function get_config(event, arg) {
        email = store.get("email")
        threads = store.get("threads")
        max_threads = os.cpus().length-1
        amd = store.get("amdGpu")
        nvidia = store.get("nvidiaGpu")
        bench = store.get("hasBenchmarked")
        event.sender.send("user_config_returned", [email, threads, max_threads, amd, nvidia, !bench] )
    }

    ipc.on("get_balance", function(event, arg) {
        let url = 'https://pickaxepool.us/get_balance_haven.php?email='+arg
        request(url, function (error, response, body) {
            if (!error && response.statusCode == 200) {
                try {
                    let json_response = JSON.parse(body);
                    event.sender.send("balance_returned", json_response)
                } catch(e) {
                    event.sender.send("balance_returned", "No balance for that email yet")
                }
            } else event.sender.send("balance_returned", error)
        })
    })

    ipc.on("get_monero_price", function(event, arg) {
        let url = 'https://api.coingecko.com/api/v3/coins/monero'
        request(url, function (error, response, body) {
            if (!error && response.statusCode == 200) {
                let json_response = JSON.parse(body);
                event.sender.send("monero_returned", json_response.market_data.current_price.usd)
            }
        })
    })

    ipc.on("withdraw", function(event, arg) {
        let url = 'https://pickaxepool.us/withdraw.php?coin=Smart%20Mining&email='+arg
        request(url, function (error, response, body) {
            if (!error && response.statusCode == 200) {
                if(body.includes("Request Sent")) 
                    event.sender.send("withdraw_response", "Check your email (and spam) for the link")
                else
                    event.sender.send("withdraw_response", "Something went wrong. Try restarting the app")
            }
        })
    })

    ipc.on("wipeit", function(event, arg) {
        const preferences = path.join(userDataPath, 'user-preferences.json');
        const config = path.join(store.get("xmrigLocation"), "config.json");
        const xmrig = path.join(store.get("xmrigLocation"), "xmrig")

        try {
            fs.unlinkSync(preferences)
            fs.unlinkSync(config)
            fs.unlinkSync(xmrig)
            app.relaunch()
            app.exit(0)
        } catch(err) {
            console.error(err)
        }
    })
}

