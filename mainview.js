let is_mining = false;
let balance = 0
let can_withdraw = 0

ipc.send("get_app_version");
ipc.on("app_version_returned", function(event, arg) {
    document.getElementById("app-version-help").innerHTML = arg[0]
    document.getElementById("miner-version-help").innerHTML = arg[1]
})


ipc.send("get_user_config");
ipc.on("user_config_returned", function(event, arg) {
    document.getElementById("email").value = arg[0]
    document.getElementById("threads").value = arg[1]
    document.getElementById("thread_value").innerHTML = arg[1]
    document.getElementById("threads").max = arg[2]

    if(arg[3] == "true") document.getElementById("amd_gpu").checked = true
    else document.getElementById("amd_gpu").checked = false

    if(arg[4] == "true") document.getElementById("nvidia_gpu").checked = true
    else document.getElementById("nvidia_gpu").checked = false

    if(arg[5] == true) document.getElementById("run_bench").checked = true
    else document.getElementById("run_bench").checked = false
})

document.getElementById("email").addEventListener('change', save_email);
function save_email(){
    ipc.send("save_email", document.getElementById("email").value);
}

document.getElementById("threads").addEventListener('change', save_threads);
function save_threads(){
    ipc.send("save_threads", document.getElementById("threads").value);
}
/*
document.getElementById("amd_gpu").addEventListener('change', save_amd);
function save_amd(){
    if (this.checked) {
        ipc.send("save_amd_gpu", "true")
      } else {
        ipc.send("save_amd_gpu", "false")
      }
}

document.getElementById("nvidia_gpu").addEventListener('change', save_nvidia);
function save_nvidia(){
    if (this.checked) {
        ipc.send("save_nvidia_gpu", "true")
      } else {
        ipc.send("save_nvidia_gpu", "false")
      }
}
*/
document.getElementById("run_bench").addEventListener('change', flip_bench);
function flip_bench(){
    if (this.checked) {
        ipc.send("set_bench", false)
      } else {
        ipc.send("set_bench", true)
      }
}

function initialize_miner() {
    if(is_mining) {
        stop_mining()
    } else {
        start_mining()
    }
}

function start_mining() {
    if(check_email()) {
        email = document.getElementById("email").value
        threads = document.getElementById("threads").value
        ipc.send("start_mining", [email, threads]);
        document.getElementById("error").style.visibility = "hidden"
        document.getElementById("shares").style.visibility = "visible"
        document.getElementById("hashrate").innerHTML = "..."
        document.getElementById("currently_mining").style.visibility = "visible"
        document.getElementById("current_coin").innerHTML = "..."
        is_mining = true;
        document.getElementById("playbutton").innerHTML = "pause"
        document.getElementById("threads").disabled = true
        document.getElementById("email").disabled = true
        //document.getElementById("amd_gpu").disabled = true
        document.getElementById("overlay").style.display = "block";
        document.getElementById("threads").style.backgroundColor = "rgb(110,110,110)"
    } else show_error("You must enter an email address to mine")
}

function check_email() {
    email = document.getElementById("email").value
    if(!email.includes("@")) return false;
    if(!email.includes(".")) return false;
    return true;
}

function show_error(error) {
    document.getElementById("error").style.visibility = "visible"
    document.getElementById("error").innerHTML = error;
}

function hide_error(error) {
    document.getElementById("error").style.visibility = "gone"
}

ipc.on("stop_mining", function(event, arg) {
    stop_mining()
})

function stop_mining() {
    ipc.send("stop_mining");
    document.getElementById("hashrate").innerHTML = "0.00"
    document.getElementById("currently_mining").style.visibility = "hidden"
    document.getElementById("hashrate").style.color = "rgb(110,110,110)"
    document.getElementById("currently_mining").style.color = "rgb(160,160,160)"
    document.getElementById("hashes_per_second").style.color = "rgb(110,110,110)"
    document.getElementById("hashes_per_second").innerHTML = "Hashes Per Second"
    document.getElementById("button").style.backgroundColor = "#ffd24c"
    document.getElementById("shares").style.color = "rgb(110,110,110)"
    document.getElementById("bench").innerHTML = "Currently Mining: "
    document.getElementById("shares").style.visibility = "hidden"
    document.getElementById("shares").innerHTML = 'Shares: <span id="share_count">0</span>'
    is_mining = false;
    document.getElementById("playbutton").innerHTML = "play_arrow"
    document.getElementById("threads").disabled = false
    document.getElementById("email").disabled = false
    //document.getElementById("amd_gpu").disabled = false
    document.getElementById("overlay").style.display = "none";
    document.getElementById("threads").style.backgroundColor = "#ffd24c"
}

ipc.on("xmrig_output", function(event, arg) {
    if(arg.includes("speed")) {
        start_index = arg.indexOf("15m ")+4
        end_index = arg.indexOf(" ", start_index)
        hashrate = arg.substring(start_index, end_index)
        if(hashrate == "n/a") hashrate = "..."
        document.getElementById("hashrate").innerHTML = hashrate
    }
    if(arg.includes("new job") || arg.includes("benchmk   Algo")) {
        if(arg.includes("cn/r")) document.getElementById("current_coin").innerHTML = "Lethean"
        if(arg.includes("cn-lite/1")) document.getElementById("current_coin").innerHTML = "Masari"
        if(arg.includes("cn-heavy/xhv")) document.getElementById("current_coin").innerHTML = "Haven"
        if(arg.includes("cn-pico")) document.getElementById("current_coin").innerHTML = "TurtleCoin"
        if(arg.includes("cn/ccx")) document.getElementById("current_coin").innerHTML = "Conceal"
        if(arg.includes("cn/gpu")) document.getElementById("current_coin").innerHTML = "Ravencoin"
        if(arg.includes("argon2/chukwav2")) document.getElementById("current_coin").innerHTML = "Argon"
        if(arg.includes("astrobwt")) document.getElementById("current_coin").innerHTML = "Dero"
        if(arg.includes("rx/0")) document.getElementById("current_coin").innerHTML = "Monero"
        if(arg.includes("rx/graft")) document.getElementById("current_coin").innerHTML = "Graft Blockchain"
        if(arg.includes("rx/arq")) document.getElementById("current_coin").innerHTML = "ArQmA"
        if(arg.includes("panthera")) document.getElementById("current_coin").innerHTML = "Scala"
    }
    if (arg.includes("STARTING ALGO PERFORMANCE CALIBRATION")) {
        document.getElementById("hashrate").style.color = "#42a5f5"
        document.getElementById("currently_mining").style.color = "#42a5f5"
        document.getElementById("hashes_per_second").style.color = "#42a5f5"
        document.getElementById("hashes_per_second").innerHTML = "Benchmarking device"
        document.getElementById("button").style.backgroundColor = "#42a5f5"
        document.getElementById("bench").innerHTML = "Testing: "
        document.getElementById("shares").style.color = "#42a5f5"
        document.getElementById("shares").innerHTML = "Benchmarking your device"
    }
    if (arg.includes("ALGO PERFORMANCE CALIBRATION COMPLETE")) {
        document.getElementById("hashrate").style.color = "#ffd24c"
        document.getElementById("currently_mining").style.color = "rgb(160,160,160)"
        document.getElementById("hashes_per_second").style.color = "rgb(110,110,110)"
        document.getElementById("hashes_per_second").innerHTML = "Hashes Per Second"
        document.getElementById("button").style.backgroundColor = "#ffd24c"
        document.getElementById("bench").innerHTML = "Currently Mining: "
        document.getElementById("shares").style.color = "rgb(110,110,110)"
        document.getElementById("shares").innerHTML = 'Shares: <span id="share_count">0</span>'
        document.getElementById("run_bench").checked = false;
        ipc.send("done_benchmarking");
    }
    if(arg.includes("accepted")) {
        start_index = arg.indexOf("(")+1
        end_index = arg.indexOf("/", start_index)
        shares = arg.substring(start_index, end_index)
        document.getElementById("share_count").innerHTML = shares
    }
    if(arg.includes("error")) {
        start_index = arg.indexOf("]")
        end_index = arg.indexOf("\"", start_index)
        error = arg.substring(start_index+2, end_index-2)
        show_error(error)
        stop_mining()
    }
})

function show_balance() {
    if(check_email()) {
        email = document.getElementById("email").value
        ipc.send("get_balance", email);
        document.getElementById("balance").innerHTML = "Loading...";
        document.getElementById("usd").innerHTML = "(...)";
        document.getElementById("balance_2").innerHTML = "...";
        document.getElementById("affiliate_earnings").innerHTML = "...";
        document.getElementById("can_withdraw").innerHTML = "...";
    } else {
        document.getElementById("balance").innerHTML = "Enter your email address";
    }
}

ipc.on("balance_returned", function(event, arg) {
    if(arg == "No balance for that email yet") {
        document.getElementById("balance").innerHTML = "No records for that email yet"
        document.getElementById("balance").style.marginTop = "300px";
        document.getElementById("balance").style.fontSize = "45px";
        document.getElementById("stuff").style.visibility = "hidden"
        return
    }

    balance = arg.mined/1000000000000
    affiliate = arg.affiliate/1000000000000
    let balanace_in_decimal = Number(balance).toFixed(8)
    let affiliate_in_decimal = Number(affiliate).toFixed(8)
    can_withdraw = Number(balance+affiliate).toFixed(8)
    if(can_withdraw < 0.00005) can_withdraw = 0;

    document.getElementById("balance").style.marginTop = "100px";
    document.getElementById("stuff").style.visibility = "visible"
    document.getElementById("balance").style.fontSize = "90px";
    document.getElementById("balance").innerHTML = balanace_in_decimal
    document.getElementById("balance_2").innerHTML = balanace_in_decimal;
    document.getElementById("affiliate_earnings").innerHTML = Number(affiliate).toFixed(8);
    document.getElementById("can_withdraw").innerHTML = Number(can_withdraw).toFixed(8);

    ipc.send("get_monero_price");
})

ipc.on("monero_returned", function(event, arg) {
    document.getElementById("usd").innerHTML = "($"+Number(balance*arg).toFixed(4)+")"
})

ipc.on("withdraw_response", function(event, arg) {
    alert(arg)
})

ipc.on("critical_error", function(event, arg) {
    alert(arg)
})

function withdraw() {
    if(check_email()) {
        if(can_withdraw > 0) {
            email = document.getElementById("email").value
            ipc.send("withdraw", email);
        } else alert("You have not mined enough to withdraw yet")
    }
}

function wipeit() {
    if (window.confirm("This will clear all settings and preferences, are you sure?")) {
        ipc.send("wipeit");
    }
}